export const SERVER_URL = `${window.location.protocol}//${
	window.location.hostname
}:5000`;

export const PRODUCTS_API = `${SERVER_URL}/api/products`;

export const CREATE_CUSTOMER_API = `${SERVER_URL}/api/customer`;

export const CREATE_DRAFT_ORDER_API = `${SERVER_URL}/api/order/checkout`;
