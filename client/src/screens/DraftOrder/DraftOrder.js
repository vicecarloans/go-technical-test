import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { DraftOrderWrapper, Title } from './DraftOrder.styles';
import { CustomerForm } from 'components/DraftOrder';
export default class DraftOrder extends Component {
	render() {
		return (
			<DraftOrderWrapper>
				<Title>Customer Information</Title>
				<CustomerForm />
			</DraftOrderWrapper>
		);
	}
}
