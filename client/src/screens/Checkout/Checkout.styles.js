import styled from 'styled-components';

export const CheckoutWrapper = styled.div`
	width: 80%;
	margin: auto;
	border-radius: 10px;
`;

export const Title = styled.h1`
	font-size: 28px;
	font-weight: bold;
	border-bottom: 1px dashed #000;
`;
