import React, { Component } from 'react';
import { CheckoutWrapper, Title } from './Checkout.styles';
import { CheckoutCart } from 'components/Checkout';

export default class Checkout extends Component {
	render() {
		return (
			<CheckoutWrapper>
				<Title>Checking out items</Title>
				<CheckoutCart />
			</CheckoutWrapper>
		);
	}
}
