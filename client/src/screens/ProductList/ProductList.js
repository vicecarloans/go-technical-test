import React, { Component } from 'react';
import { Container } from './ProductList.styles';
import classNames from 'classnames';
import { ProductDisplay } from 'components/ProductList';
import { Fab, withStyles, Snackbar, Button, Badge } from '@material-ui/core';
import { connect } from 'react-redux';
import { closeNotifications } from 'flux/ducks/carts';
import { ShoppingCart } from '@material-ui/icons';

const styles = theme => ({
	fab: {
		margin: theme.spacing.unit + 12,
		position: 'fixed',
		bottom: theme.spacing.unit * 2,
		right: theme.spacing.unit * 2,
		zIndex: 100,
	},
	fabMoveUp: {
		transform: 'translate3d(0, -46px, 0)',
		transition: theme.transitions.create('transform', {
			duration: theme.transitions.duration.enteringScreen,
			easing: theme.transitions.easing.easeOut,
		}),
	},
	fabMoveDown: {
		transform: 'translate3d(0, 0, 0)',
		transition: theme.transitions.create('transform', {
			duration: theme.transitions.duration.leavingScreen,
			easing: theme.transitions.easing.sharp,
		}),
	},
	snackbar: {
		position: 'fixed',
	},
	snackbarContent: {
		width: 450,
	},
	margin: {
		margin: theme.spacing.unit * 2,
	},
});

class ProductList extends Component {
	handleClose = () => {
		this.props.closeNotifications();
	};
	render() {
		const { classes, history, notifications, count } = this.props;
		const fabClassName = classNames(
			classes.fab,
			notifications ? classes.fabMoveUp : classes.fabMoveDown
		);
		return (
			<Container>
				<ProductDisplay />
				<Fab
					color="primary"
					onClick={() => history.push('/checkout')}
					className={fabClassName}
				>
					<Badge
						className={classes.margin}
						badgeContent={count}
						color="secondary"
					>
						<ShoppingCart />
					</Badge>
				</Fab>
				<Snackbar
					open={notifications}
					onClose={this.handleClose}
					autoHideDuration={3000}
					ContentProps={{
						'aria-describedby': 'snackbar-fab-message-id',
						className: classes.snackbarContent,
					}}
					message={
						<span id="snackbar-fab-message-id">
							Item saved to cart
						</span>
					}
					action={
						<Button
							color="inherit"
							size="small"
							onClick={this.handleClose}
						>
							Close
						</Button>
					}
					className={classes.snackbar}
				/>
			</Container>
		);
	}
}

const MapStateToProps = state => ({
	notifications: state.carts.notifications,
	count: state.carts.items.allIds.reduce((acc, val) => {
		return acc + Number(state.carts.items.byIds[val].quantity);
	}, 0),
});

const MapDispatchToProps = {
	closeNotifications,
};

ProductList = withStyles(styles)(ProductList);

export default connect(
	MapStateToProps,
	MapDispatchToProps
)(ProductList);
