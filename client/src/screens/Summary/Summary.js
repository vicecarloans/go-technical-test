import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
	SummaryWrapper,
	Title,
	SummaryDetailWrapper,
	BillWrapper,
	TotalWrapper,
} from './Summary.styles';
import { restart } from 'flux/ducks/carts';
import { connect } from 'react-redux';
import { Button } from '@material-ui/core';

class Summary extends Component {
	handleRestart = () => {
		this.props.restart();
		this.props.history.push('/');
	};
	render() {
		return (
			<SummaryWrapper>
				<Title>Summary</Title>
				<SummaryDetailWrapper>
					<BillWrapper>
						<TotalWrapper>Draft Order Id: </TotalWrapper>
						<TotalWrapper>
							{this.props.summary.draftId}
						</TotalWrapper>
					</BillWrapper>
					<BillWrapper>
						<TotalWrapper>Subtotal: </TotalWrapper>
						<TotalWrapper>
							${this.props.summary.subtotal}
						</TotalWrapper>
					</BillWrapper>
					<BillWrapper>
						<TotalWrapper>Tax: </TotalWrapper>
						<TotalWrapper>
							${this.props.summary.totalTax}
						</TotalWrapper>
					</BillWrapper>
					<BillWrapper>
						<TotalWrapper>Total: </TotalWrapper>
						<TotalWrapper>
							${this.props.summary.totalPrice}
						</TotalWrapper>
					</BillWrapper>
					<Button
						variant="contained"
						color="primary"
						style={{ margin: 'auto' }}
						onClick={this.handleRestart}
					>
						Restart
					</Button>
				</SummaryDetailWrapper>
			</SummaryWrapper>
		);
	}
}

const MapStateToProps = state => ({
	summary: state.carts.summary,
});

const MapDispatchToProps = {
	restart,
};

export default connect(
	MapStateToProps,
	MapDispatchToProps
)(Summary);
