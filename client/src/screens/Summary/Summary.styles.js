import styled from 'styled-components';

export const SummaryWrapper = styled.div`
	width: 80%;
	margin: auto;
	border-radius: 10px;
`;

export const Title = styled.h1`
	font-size: 28px;
	font-weight: bold;
	border-bottom: 1px dashed #000;
`;

export const SummaryDetailWrapper = styled.div`
	text-align: center;
	width: 100%;
`;

export const TotalWrapper = styled.h1`
	flex: 0.5;
	font-size: 20px;
	font-weight: bold;
`;

export const BillWrapper = styled.div`
	display: flex;
	justify-content: space-between;
`;
