import React, { Component } from 'react';
import {
	BrowserRouter as Router,
	Route,
	Switch,
	Redirect,
} from 'react-router-dom';
import { MainContainer } from './App.styles';
import { UniversalHeader } from 'components/common';
import ProductList from './screens/ProductList';
import { connect } from 'react-redux';
import CheckoutScreen from './screens/Checkout';
import DraftOrderScreen from './screens/DraftOrder';
import SummaryScreen from './screens/Summary';

const PrivateRoute = ({ component: Component, ...rest }) => (
	<Route
		{...rest}
		render={props =>
			rest.canCheckout ? <Component {...props} /> : <Redirect to="/" />
		}
	/>
);

class App extends Component {
	render() {
		return (
			<Router>
				<MainContainer>
					<UniversalHeader />
					<Switch>
						<Route exact path="/" component={ProductList} />
						<Route
							exact
							path="/checkout"
							component={CheckoutScreen}
						/>
						<PrivateRoute
							exact
							path="/checkout/info"
							component={DraftOrderScreen}
							canCheckout={this.props.items.allIds.length > 0}
						/>
						<Route
							exact
							path="/checkout/summary"
							component={SummaryScreen}
						/>
					</Switch>
				</MainContainer>
			</Router>
		);
	}
}

const MapStateToProps = state => ({
	items: state.carts.items,
});

export default connect(MapStateToProps)(App);
