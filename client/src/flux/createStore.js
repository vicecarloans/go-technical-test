import { createStore, applyMiddleware } from 'redux';

import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import thunk from 'redux-thunk';

import { persistStore, persistReducer } from 'redux-persist';

import storage from 'redux-persist/lib/storage';

import rootReducer from './rootReducer';

const persistConfig = {
	key: 'Go-Integrations-Test',
	storage,
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

export default () => {
	const store = createStore(
		persistedReducer,
		{},
		composeWithDevTools(applyMiddleware(thunk))
	);

	const persistor = persistStore(store);

	return { store, persistor };
};
