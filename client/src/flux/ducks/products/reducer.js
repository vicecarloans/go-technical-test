import {
	FETCH_PRODUCTS_SUCCESS,
	FETCH_PRODUCTS,
	FETCH_PRODUCTS_FAILED,
} from './actions';

const initialState = {
	collections: null,
	loading: false,
};

export default (state = initialState, { type, payload }) => {
	switch (type) {
		case FETCH_PRODUCTS:
			return { ...state, loading: true };
		case FETCH_PRODUCTS_SUCCESS:
			return { ...state, collections: payload.products, loading: false };
		case FETCH_PRODUCTS_FAILED:
			return { ...state, collections: payload.products, loading: false };
		default:
			return state;
	}
};
