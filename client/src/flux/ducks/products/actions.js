import Axios from 'axios';
import { PRODUCTS_API } from 'constants/api';
export const FETCH_PRODUCTS = 'FETCH_PRODUCTS';

export const fetchProduct = () => async dispatch => {
	dispatch({
		type: FETCH_PRODUCTS,
	});
	try {
		const res = await Axios.get(PRODUCTS_API);
		dispatch(fetchProductSuccess(res.data));
	} catch (err) {
		dispatch(fetchProductFailed(err));
	}
};

export const FETCH_PRODUCTS_SUCCESS = 'FETCH_PRODUCTS_SUCCESS';

export const fetchProductSuccess = products => ({
	type: FETCH_PRODUCTS_SUCCESS,
	payload: {
		products,
	},
});

export const FETCH_PRODUCTS_FAILED = 'FETCH_PRODUCTS_FAILED';

export const fetchProductFailed = err => ({
	type: FETCH_PRODUCTS_FAILED,
	payload: {
		err,
	},
});
