import {
	ADD_PRODUCT_TO_CART,
	CLOSE_NOTIFICATIONS,
	UPDATE_QUANTITY,
	DELETE_ITEM,
	CREATE_DRAFT_ORDER,
	CREATE_DRAFT_ORDER_SUCCESS,
	CREATE_DRAFT_ORDER_FAILED,
	RESTART,
} from './actions';

const initialState = {
	items: { byIds: {}, allIds: [] },

	notifications: false,

	loading: false,

	err: null,

	summary: {},
};

const generateNewItems = (items, product) => {
	const quantity = items.byIds[product.productId]
		? items.byIds[product.productId].quantity
		: 0;
	const newProduct = {
		[product.productId]: {
			...product,
			quantity: !isNaN(quantity) && quantity + 1,
		},
	};

	const previousIds = items.allIds.length > 0 ? items.allIds : [];
	const newItems = {
		byIds: {
			...items.byIds,
			...newProduct,
		},
		allIds: [...new Set([...previousIds, product.productId])],
	};
	return newItems;
};

export default (state = initialState, { type, payload }) => {
	switch (type) {
		case ADD_PRODUCT_TO_CART: {
			const { items } = state;

			const newItems = generateNewItems(items, payload.product);
			return {
				...state,
				items: newItems,
				notifications: true,
			};
		}

		case CLOSE_NOTIFICATIONS:
			return { ...state, notifications: false };

		case UPDATE_QUANTITY: {
			const { items } = state;
			const newItems = { ...items };
			newItems.byIds[payload.productId].quantity = Number(
				payload.quantity
			);
			return {
				...state,
				items: newItems,
			};
		}

		case DELETE_ITEM: {
			const { items } = state;
			const newItems = { ...items };
			delete newItems.byIds[payload.productId];
			newItems.allIds.splice(
				newItems.allIds.indexOf(payload.productId),
				1
			);
			return { ...state, items: newItems };
		}

		case CREATE_DRAFT_ORDER:
			return { ...state, loading: true };
		case CREATE_DRAFT_ORDER_SUCCESS:
			return { ...state, loading: false, summary: payload.summary };
		case CREATE_DRAFT_ORDER_FAILED:
			return { ...state, loading: false, err: payload.err };

		case RESTART:
			return initialState;
		default:
			return state;
	}
};
