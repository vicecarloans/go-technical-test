import axios from 'axios';
import { CREATE_CUSTOMER_API, CREATE_DRAFT_ORDER_API } from 'constants/api';
import { destroy } from 'redux-form';

export const ADD_PRODUCT_TO_CART = 'ADD_PRODUCT_TO_CART';

export const addToCard = product => ({
	type: ADD_PRODUCT_TO_CART,
	payload: {
		product,
	},
});

export const CLOSE_NOTIFICATIONS = 'CLOSE_NOTIFICATIONS';

export const closeNotifications = () => ({
	type: CLOSE_NOTIFICATIONS,
});

export const UPDATE_QUANTITY = 'UPDATE_QUANTITY';

export const updateQuantity = (productId, quantity) => ({
	type: UPDATE_QUANTITY,
	payload: {
		productId,
		quantity,
	},
});

export const DELETE_ITEM = 'DELETE_ITEM';

export const deleteItem = productId => ({
	type: DELETE_ITEM,
	payload: {
		productId,
	},
});

export const CREATE_DRAFT_ORDER = 'CREATE_DRAFT_ORDER';

export const createDraftOrder = ({ customer, items }) => async dispatch => {
	dispatch({
		type: CREATE_DRAFT_ORDER,
	});
	try {
		console.log(customer);
		const {
			data: { shopifyId },
		} = await axios.post(CREATE_CUSTOMER_API, customer);
		const {
			data: { draftId, subtotal, totalTax, totalPrice },
		} = await axios.post(CREATE_DRAFT_ORDER_API, {
			items,
			shopifyId,
		});

		console.log(draftId);
		const summary = { draftId, subtotal, totalTax, totalPrice };
		dispatch(createDraftOrderSuccess(summary));
		dispatch(destroy('customer'));
		window.location.assign('/checkout/summary');
	} catch (err) {
		dispatch(createDraftOrderFailed(err));
	}
};

export const CREATE_DRAFT_ORDER_SUCCESS = 'CREATE_DRAFT_ORDER_SUCCESS';

export const createDraftOrderSuccess = summary => ({
	type: CREATE_DRAFT_ORDER_SUCCESS,
	payload: {
		summary,
	},
});

export const CREATE_DRAFT_ORDER_FAILED = 'CREATE_DRAFT_ORDER_FAILED';

export const createDraftOrderFailed = err => ({
	type: CREATE_DRAFT_ORDER_FAILED,
	payload: {
		err,
	},
});

export const RESTART = 'RESTART';

export const restart = payload => ({
	type: RESTART,
	payload,
});
