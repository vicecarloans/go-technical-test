import { combineReducers } from 'redux';

import { reducer as formReducer } from 'redux-form';
import productsReducer from './ducks/products';
import cartsReducer from './ducks/carts';

const rootReducer = combineReducers({
	form: formReducer,
	products: productsReducer,
	carts: cartsReducer,
});

export default rootReducer;
