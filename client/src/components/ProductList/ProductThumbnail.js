import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
	withStyles,
	GridList,
	GridListTile,
	GridListTileBar,
	ListSubheader,
	IconButton,
} from '@material-ui/core';
import { AddShoppingCart } from '@material-ui/icons';
import { productStyles, ProductImage } from './ProductList.styles';
import { connect } from 'react-redux';
import { addToCard } from 'flux/ducks/carts';

class ProductThumbnail extends Component {
	static propTypes = {
		product: PropTypes.object.isRequired,
	};

	handleAddToCard = () => {
		this.props.addToCard(this.props.product);
	};

	render() {
		const { classes, product } = this.props;
		const { image, title, price } = product;
		return (
			<GridListTile className={classes.gridTile}>
				<ProductImage image={image} />
				<GridListTileBar
					title={title}
					subtitle={
						<span>
							<span
								style={{
									textDecoration: 'line-through',
									marginRight: '5px',
								}}
							>
								${price}
							</span>
							${(Number(price) * 0.75).toFixed(2)}
						</span>
					}
					actionIcon={
						<IconButton
							onClick={this.handleAddToCard}
							className={classes.icon}
						>
							<AddShoppingCart />
						</IconButton>
					}
				/>
			</GridListTile>
		);
	}
}

ProductThumbnail = withStyles(productStyles)(ProductThumbnail);

const MapDispatchToProps = {
	addToCard,
};

export default connect(
	null,
	MapDispatchToProps
)(ProductThumbnail);
