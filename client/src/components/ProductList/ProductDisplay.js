import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { fetchProduct } from 'flux/ducks/products';
import { productStyles } from './ProductList.styles';
import { withStyles, GridList } from '@material-ui/core';
import ProductThumbnail from './ProductThumbnail';

export class ProductDisplay extends Component {
	static propTypes = {
		collections: PropTypes.shape({
			products: PropTypes.array,
			offset: PropTypes.number,
			count: PropTypes.number,
		}),
		fetchProduct: PropTypes.func.isRequired,
	};

	static defaultProps = {
		collections: {},
	};

	componentDidMount() {
		this.props.fetchProduct();
	}
	renderProductThumbnail = () => {
		return this.props.collections.products && this.props.collections.products.map(p => (
			<ProductThumbnail key={p.productId} product={p} />
		));
	};

	render() {
		const { classes, collections } = this.props;
		return (
			<div className={classes.root}>
				{collections && (
					<GridList className={classes.gridList}>
						{this.renderProductThumbnail()}
					</GridList>
				)}
			</div>
		);
	}
}

const mapStateToProps = state => ({
	collections: state.products.collections,
	loading: state.products.loading,
});

const mapDispatchToProps = {
	fetchProduct,
};

ProductDisplay = withStyles(productStyles)(ProductDisplay);

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(ProductDisplay);
