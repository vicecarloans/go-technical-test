import styled from 'styled-components';

export const ProductImage = styled.div`
	width: 325px;
	height: 325px;
	background-image: url(${props => props.image});
	background-size: contain;
	background-position: center center;
	background-repeat: no-repeat;

	@media (max-width: 700px) {
		width: 250px;
		height: 250px;
	}
`;

export const productStyles = theme => ({
	root: {
		display: 'flex',
		flexWrap: 'wrap',
		justifyContent: 'space-around',
		overflow: 'hidden',
	},
	gridList: {
		width: 700,
		justifyContent: 'space-between',

		[theme.breakpoints.down('sm')]: {
			width: 300,
		},
	},
	gridTile: {
		margin: 10,
	},
	icon: {
		color: 'rgba(255, 255, 255, 0.54)',
	},
});
