import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { updateQuantity, deleteItem } from 'flux/ducks/carts';
import {
	CartHeader,
	ProductTitle,
	ProductInfoWrapper,
	ProductInfoTitle,
	CartAction,
	BillDiv,
	TotalWrapper,
	BillWrapper,
} from './Checkout.styles';
import ProductCard from './ProductCard';
import { calculateBill, calculateTax, calculateTotal } from 'utils/price';
import { withRouter } from 'react-router-dom';

import { Button, withStyles } from '@material-ui/core';

const styles = theme => ({
	button: {
		margin: theme.spacing.unit,
	},
});

export class CheckoutCart extends Component {
	static propTypes = {
		items: PropTypes.object.isRequired,
	};
	state = {
		subtotal: 0,
		tax: 0,
		total: 0,
	};
	renderCart = () => {
		return this.props.items.allIds.map(id => (
			<ProductCard
				key={id}
				product={this.props.items.byIds[id]}
				onDeleteProduct={() => this.props.deleteItem(id)}
				onUpdateProductQuantity={qty =>
					this.props.updateQuantity(id, qty)
				}
			/>
		));
	};

	static getDerivedStateFromProps(props, state) {
		let newState = {};
		if (props.items && props.items.allIds.length > 0) {
			const products = props.items.allIds.map(
				id => props.items.byIds[id]
			);
			const subtotal = calculateBill(products);
			const tax = calculateTax(subtotal);
			const total = calculateTotal(subtotal, tax);
			newState = { subtotal, tax, total };
		}
		return newState;
	}
	render() {
		const { subtotal, tax, total } = this.state;
		const { classes } = this.props;
		return this.props.items.allIds.length > 0 ? (
			<div>
				<CartHeader>
					<ProductTitle>Product</ProductTitle>
					<ProductInfoWrapper>
						<ProductInfoTitle>Qty</ProductInfoTitle>
						<ProductInfoTitle>Subtotal</ProductInfoTitle>
					</ProductInfoWrapper>
				</CartHeader>
				{this.renderCart()}
				<CartAction>
					<BillDiv>
						<Button
							variant="contained"
							color="primary"
							className={classes.button}
							onClick={() =>
								this.props.history.push('/checkout/info')
							}
						>
							Proceed
						</Button>
					</BillDiv>
				</CartAction>
			</div>
		) : (
			<Fragment>
				<div>You have no items in your cart right now</div>
				<Button
					variant="contained"
					color="primary"
					className={classes.button}
					onClick={() => this.props.history.push('/')}
				>
					Click here to continue shopping
				</Button>
			</Fragment>
		);
	}
}

const mapStateToProps = state => ({
	items: state.carts.items,
});

const mapDispatchToProps = {
	updateQuantity,
	deleteItem,
};

CheckoutCart = withRouter(CheckoutCart);
CheckoutCart = withStyles(styles)(CheckoutCart);

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(CheckoutCart);
