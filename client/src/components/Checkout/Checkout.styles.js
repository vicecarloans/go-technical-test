import styled from 'styled-components';

export const CartHeader = styled.div`
	width: 100%;
	display: flex;
	justify-content: space-between;
	border-bottom: 1px solid #000;
`;

export const ProductTitle = styled.h1`
	font-size: 20px;
	font-weight: bold;
	flex: 0.7;
	display: inline;
`;

export const ProductInfoWrapper = styled.span`
	flex: 0.3;
	display: flex;
	justify-content: space-between;
`;

export const ProductInfoTitle = styled.h1`
	font-size: 20px;
	font-weight: bold;
	flex: 0.5;
`;

export const ProductCardDiv = styled.div`
	width: 100%;
	display: flex;
	flex-wrap: wrap;
	justify-content: space-between;
`;

export const ProductCardInfoDiv = styled.div`
	flex: 0.7;
	display: flex;
`;

export const ProductImage = styled.div`
	width: 250px;
	height: 250px;
	background-image: url(${props => props.picture});
	background-position: center center;
	background-size: contain;
	background-repeat: no-repeat;
`;

export const ProductDescriptionDiv = styled.div`
	width: 100%;
	display: flex;
	flex-direction: column;
	justify-content: center;
	margin-left: 20px;
	flex-wrap: wrap;
`;

export const ProductSubtotal = styled.h1`
	font-size: 18px;
	font-weight: 600;
	flex: 0.5;
`;

export const ProductCardSummaryDiv = styled.div`
	flex: 0.3;
	display: flex;
	justify-content: space-between;
	align-items: center;
`;

export const ProductQuantity = styled.div`
	flex: 0.5;
`;

export const Title = styled.h1`
	font-size: 20px;
	font-weight: bold;
`;

export const NormalText = styled.p`
	font-size: 14px;
`;

export const OriginalPrice = styled(NormalText)`
	display: inline;
	font-weight: bold;
	text-decoration: line-through;
`;
export const Price = styled(OriginalPrice)`
	font-weight: 600;
	text-decoration: none;
	padding-left: 10px;
`;

export const CartAction = styled.div`
	display: flex;
	justify-content: flex-end;
`;

export const BillDiv = styled.div`
	flex: 0.3;
	display: flex;
	flex-direction: column;
	justify-content: space-between;
`;

export const TotalWrapper = styled(Title)`
	flex: 0.5;
`;

export const BillWrapper = styled.div`
	display: flex;
	justify-content: space-between;
`;
