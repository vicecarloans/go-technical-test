import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import {
	ProductCardDiv,
	ProductCardInfoDiv,
	ProductImage,
	ProductDescriptionDiv,
	Title,
	NormalText,
	OriginalPrice,
	Price,
	ProductCardSummaryDiv,
	ProductQuantity,
	ProductSubtotal,
} from './Checkout.styles';
import { getDiscountPrice, calculateSubtotal } from 'utils/price';
import { TextField, withStyles, IconButton } from '@material-ui/core';
import { Close } from '@material-ui/icons';

const styles = theme => ({
	textField: {
		marginLeft: theme.spacing.unit,
		marginRight: theme.spacing.unit,
		width: 50,
	},
	deleteBtn: {
		top: 0,
		right: 0,
	},
});

class ProductCard extends PureComponent {
	static propTypes = {
		product: PropTypes.object.isRequired,
		onDeleteProduct: PropTypes.func.isRequired,
		onUpdateProductQuantity: PropTypes.func.isRequired,
	};

	handleQuantityChange = e => {
		if (Number(e.target.value) > 0) {
			this.props.onUpdateProductQuantity(e.target.value);
		}
	};

	render() {
		const { product, classes } = this.props;
		return (
			<ProductCardDiv>
				<ProductCardInfoDiv>
					<ProductImage picture={product.image} />
					<ProductDescriptionDiv>
						<Title>
							{product.title}{' '}
							<IconButton
								aria-label="Close"
								className={classes.deleteBtn}
								onClick={this.props.onDeleteProduct}
							>
								<Close />
							</IconButton>
						</Title>
						<NormalText>{product.description}</NormalText>
						<div>
							<OriginalPrice>${product.price}</OriginalPrice>
							<Price>${getDiscountPrice(product.price)}</Price>
						</div>
					</ProductDescriptionDiv>
				</ProductCardInfoDiv>
				<ProductCardSummaryDiv>
					<ProductQuantity>
						<TextField
							id="outlined-number"
							label="Number"
							value={product.quantity}
							onChange={this.handleQuantityChange}
							type="number"
							className={classes.textField}
							InputLabelProps={{
								shrink: true,
							}}
							margin="normal"
							variant="outlined"
						/>
					</ProductQuantity>
					<ProductSubtotal>
						${calculateSubtotal(product.price, product.quantity)}
					</ProductSubtotal>
				</ProductCardSummaryDiv>
			</ProductCardDiv>
		);
	}
}

export default withStyles(styles)(ProductCard);
