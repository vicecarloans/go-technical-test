import styled from 'styled-components';

export const Wrapper = styled.header`
	padding: 0.5rem 1rem;
	width: 100%;
	display: flex;
	align-items: center;
	flex-direction: row;
	justify-content: center;
	flex-wrap: nowrap;
	max-height: 60px;
	z-index: 51;
`;

export const WebLogo = styled.h1`
	color: #000;
	font-style: italic;
	font-weight: bold;
	font-size: 32px;
`;
