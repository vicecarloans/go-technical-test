import React, { PureComponent } from 'react';
import { Wrapper, WebLogo } from './UniversalHeader.styles';
import { Link } from 'react-router-dom';

export default class UniversalHeader extends PureComponent {
	render() {
		return (
			<Wrapper>
				<Link to="/">
					<WebLogo>My Thrift Shop</WebLogo>
				</Link>
			</Wrapper>
		);
	}
}
