import styled from 'styled-components';

export const NavigationButtonWrapper = styled.div`
	width: 100%;
	display: flex;
	justify-content: center;
	margin: 20px 0;
`;

export const CustomerFormWrapper = styled.form`
	display: flex;
	flex-direction: column;
`;
