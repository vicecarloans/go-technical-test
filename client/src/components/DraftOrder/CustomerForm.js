import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { reduxForm, Field, isInvalid } from 'redux-form';
import { withRouter } from 'react-router-dom';
import { validateSync } from 'utils/validation';
import {
	TextField,
	withStyles,
	Button,
	MenuItem,
	CircularProgress,
} from '@material-ui/core';
import { PROVINCES, COUNTRIES } from 'constants/menus';
import {
	NavigationButtonWrapper,
	CustomerFormWrapper,
} from './DraftOrder.styles.js';
import { getDiscountPrice } from 'utils/price';

import { createDraftOrder } from 'flux/ducks/carts';

const onSubmit = (values, dispatch, props) => {
	const { items } = props;
	const lineItems = items.allIds.map(id => {
		return {
			title: items.byIds[id].title,
			price: getDiscountPrice(items.byIds[id].price),
			quantity: items.byIds[id].quantity,
		};
	});
	dispatch(createDraftOrder({ customer: values, items: lineItems }));
};

const styles = theme => ({
	formControl: {
		margin: theme.spacing.unit,
	},
	menu: {
		width: 200,
	},
	wrapper: {
		margin: theme.spacing.unit,
		position: 'relative',
	},
	buttonProgress: {
		position: 'absolute',
		top: '50%',
		left: '50%',
		marginTop: -12,
		marginLeft: -12,
	},
});

export class CustomerForm extends Component {
	static propTypes = {
		isInvalid: PropTypes.bool.isRequired,
	};

	renderField = ({
		input,
		placeholder,
		label,
		type,
		menus,
		meta: { asyncValidating, touched, error },
	}) => (
		<Fragment>
			{type === 'dropdown' ? (
				<TextField
					id={input.name}
					error={touched && !!error}
					select
					label={label}
					className={this.props.classes.textField}
					value={input.value}
					onChange={input.onChange}
					SelectProps={{
						MenuProps: {
							className: this.props.classes.menu,
						},
					}}
					helperText={
						touched && !!error ? error : `Select your ${input.name}`
					}
					margin="normal"
					variant="outlined"
				>
					{menus.map(option => (
						<MenuItem key={option} value={option}>
							{option}
						</MenuItem>
					))}
				</TextField>
			) : (
				<TextField
					id={input.name}
					error={touched && !!error}
					label={label}
					className={this.props.classes.textField}
					value={input.value}
					onChange={input.onChange}
					helperText={touched && !!error && error}
					margin="normal"
					variant="outlined"
				/>
			)}
		</Fragment>
	);

	handleClick = () => {
		this.props.submit('customer');
	};
	render() {
		const { handleSubmit, pristine, reset, submitting } = this.props;
		return (
			<CustomerFormWrapper onSubmit={handleSubmit}>
				<Field
					component={this.renderField}
					name="email"
					type="text"
					placeholder="e.g. john.doe@gmail.com"
					label="Email"
				/>
				<Field
					component={this.renderField}
					name="firstName"
					type="text"
					placeholder="e.g. John"
					label="First Name"
				/>
				<Field
					component={this.renderField}
					name="lastName"
					type="text"
					placeholder="e.g. Doe"
					label="Last Name"
				/>
				<Field
					component={this.renderField}
					name="address1"
					type="text"
					placeholder="e.g. 100 King Street West"
					label="Address"
				/>
				<Field
					component={this.renderField}
					name="address2"
					type="text"
					placeholder="e.g. Suite 100"
					label="Apartment"
				/>
				<Field
					component={this.renderField}
					name="city"
					type="text"
					placeholder="e.g. Toronto"
					label="City"
				/>
				<Field
					component={this.renderField}
					name="province"
					type="dropdown"
					placeholder="e.g. ON"
					label="Province"
					menus={PROVINCES}
				/>
				<Field
					component={this.renderField}
					name="country"
					type="dropdown"
					placeholder="e.g. CA"
					label="Country"
					menus={COUNTRIES}
				/>
				<Field
					component={this.renderField}
					name="zip"
					type="text"
					placeholder="e.g. A1B C2D"
					label="Postal Code"
				/>
				<NavigationButtonWrapper>
					<div className={this.props.classes.wrapper}>
						<Button
							onClick={this.handleClick}
							disabled={
								this.props.isInvalid || this.props.loading
							}
							variant="contained"
							color="primary"
						>
							Create Order
						</Button>
						{this.props.loading && (
							<CircularProgress
								size={24}
								className={this.props.classes.buttonProgress}
							/>
						)}
					</div>
				</NavigationButtonWrapper>
			</CustomerFormWrapper>
		);
	}
}

CustomerForm = reduxForm({
	form: 'customer',
	validate: validateSync,
	onSubmit,
	destroyOnUnmount: false,
})(CustomerForm);

CustomerForm = withStyles(styles)(CustomerForm);

CustomerForm = withRouter(CustomerForm);

const mapStateToProps = state => ({
	isInvalid: isInvalid('register')(state),
	items: state.carts.items,
	loading: state.carts.loading,
});

const mapDispatchToProps = {};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(CustomerForm);
