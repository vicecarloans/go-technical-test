export const getDiscountPrice = originalPrice =>
	(Number(originalPrice) * 0.75).toFixed(2);

export const calculateSubtotal = (price, quantity) =>
	(Number(getDiscountPrice(price)) * quantity).toFixed(2);

export const calculateBill = products => {
	return products.reduce((acc, val) => {
		return acc + val.quantity * Number(getDiscountPrice(val.price));
	}, 0.0);
};

export const calculateTax = subtotal => Number(subtotal) * 0.13;

export const calculateTotal = (subtotal, tax) => Number(subtotal) + Number(tax);
