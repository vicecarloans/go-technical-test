export const validateSync = values => {
	const errors = {};
	if (!values.email) {
		errors.email = 'Required';
	} else if (
		!/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
			values.email
		)
	) {
		errors.email = 'Invalid email address';
	}
	if (!values.firstName) {
		errors.firstName = 'Required';
	}
	if (!values.lastName) {
		errors.lastName = 'Required';
	}
	if (!values.address1) {
		errors.address1 = 'Required';
	}
	if (!values.city) {
		errors.city = 'Required';
	}
	if (!values.country) {
		errors.country = 'Required';
	}
	if (!values.province) {
		errors.province = 'Required';
	}
	if (!values.zip) {
		errors.zip = 'Required';
	}
	return errors;
};
