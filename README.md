# Go Integrations Technical Test

The application is available at [here](http://ec2-54-210-202-153.compute-1.amazonaws.com/)

## Running Development

1. Clone the repo
2. Run `yarn` to install dependencies
3. There are couple of options for this:
    - No docker:
        1. Install MongoDB and its driver. Preferably using a docker container of mongo for this.
        2. Add `dev.js` file inside `config` folder that has all of attributes like `prod.js`
        3. Run `yarn run dev` at root
    - With docker:
        1. Add credentials inside `docker-compose.yml`
        2. Change directory to `client` then run `yarn run build`
        3. Go to `root` and run `docker-compose up -d`
        4. Configure MongoDB container to createUser. More details are [here](https://techsparx.com/software-development/docker/damp/mongodb.html)
        ```
        > mongo --port 27107
        > use technical-test-db
        > db.createUser({user: "admin", pwd: "admin", roles: [{role: "readWrite", db: "technical-test-db"}]})
        ```
        5. Run `docker-compose restart` again and go to [localhost](http://localhost:80)
