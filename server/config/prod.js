module.exports = {
	mongoURI: process.env.MONGO_URI,
	shopName: process.env.SHOP_NAME,
	shopifyAPI: process.env.SHOPIFY_API,
	shopifyPassword: process.env.SHOPIFY_PASSWORD,
};
