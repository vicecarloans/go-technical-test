const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const ProductModel = mongoose.model('product');

router.get('/', async (req, res, next) => {
	const offset = req.query.offset || 0;
	const limit = req.query.limit || 50;

	try {
		const products = await ProductModel.find()
			.limit(limit)
			.skip(offset * limit);
		const count = await ProductModel.count();

		res.json({ products, offset, count });
	} catch (err) {
		next({ status: 400, message: err.message });
	}
});

router.get('/:id', async (req, res, next) => {
	const productId = req.params.id;
	try {
		const product = await ProductModel.findById(productId);
		res.json(product);
	} catch (err) {
		next({ status: 400, message: err.message });
	}
});

module.exports = router;
