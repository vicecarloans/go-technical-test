const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

const CustomerModel = mongoose.model('customer');

// /api/customer
router.post('/', async (req, res, next) => {
	try {
		const {
			email,
			firstName,
			lastName,
			address1,
			address2,
			city,
			country,
			province,
			zip,
		} = req.body;

		const address = {
			address1,
			address2,
			city,
			country,
			province,
			zip,
		};

		const data = await global.shopify.customer.create({
			email,
			first_name: firstName,
			last_name: lastName,
			addresses: [address],
		});

		const Customer = new CustomerModel({
			email,
			firstName,
			lastName,
			addresses: [address],
			shopifyId: data.id,
		});

		await Customer.save();

		res.status(201).json({
			message: 'Customer created successfully',
			shopifyId: data.id,
		});
	} catch (err) {
		next({ status: 400, message: err.message });
	}
});

module.exports = router;
