const express = require('express');
const router = express.Router();

router.post('/checkout', async (req, res, next) => {
	try {
		const { items, shopifyId } = req.body;

		const data = await global.shopify.draftOrder.create({
			line_items: items,
			customer: {
				id: shopifyId,
			},
			use_customer_default_address: true,
		});

		res.status(201).json({
			message: 'Draft was created successfully',
			draftId: data.id,
			subtotal: data.subtotal_price,
			totalTax: data.total_tax,
			totalPrice: data.total_price,
		});
	} catch (err) {
		next({ status: 400, error: err.message });
	}
});

module.exports = router;
