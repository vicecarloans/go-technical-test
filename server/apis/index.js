const express = require('express');
const api = express.Router();
const productRoutes = require('./product');
const customerRoutes = require('./customer');
const orderRoutes = require('./order');

api.use('/products', productRoutes);
api.use('/customer', customerRoutes);
api.use('/order', orderRoutes);

module.exports = api;
