const express = require('express');

// Promisify Mongoose
const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
const cors = require('cors');
const helmet = require('helmet');
const bodyParser = require('body-parser');
const Shopify = require('shopify-api-node');
const keys = require('./config/keys');
const shopify = new Shopify({
	shopName: keys.shopName,
	apiKey: keys.shopifyAPI,
	password: keys.shopifyPassword,
});

global.shopify = shopify;
//migrations db
require('./migrations')();

const PORT = process.env.PORT || 5000;
const app = express();

// Setting up middleware/extensions
app.use(helmet());
app.use(bodyParser.json());

// Setting up cors
const corsOptions = {
	origin: '*',
	optionsSuccessStatus: 200,
};

app.use(cors(corsOptions));

// Route
app.use('/api', require('./apis'));

app.use((err, req, res, next) => {
	res.status(err.status || 500).json({ error: err });
});

app.listen(PORT, () => {
	console.log(`Server started on port ${PORT}`);
});
