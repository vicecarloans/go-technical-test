const mongoose = require('mongoose');
const keys = require('../config/keys');

module.exports = () => {
	require('./models/ProductSchema');
	require('./models/CustomerSchema');
	mongoose.connect(keys.mongoURI);
	mongoose.connection.on('connected', async () => {
		try {
			const products = await global.shopify.product.list({});
			const ProductModel = mongoose.model('product');
			if (!products) {
				throw new Error('Response Data is null');
			}
			products.forEach(async p => {
				try {
					const {
						variants,
						id,
						title,
						body_html,
						image: { src },
					} = p;
					const price = variants.length > 0 && variants[0].price;
					const Product = new ProductModel({
						productId: id,
						title: title,
						description: body_html,
						price,
						image: src,
					});
					await Product.save();
				} catch (err) {
					console.warn(err.message);
				}
			});
		} catch (err) {
			console.warn(err.message);
		}
	});
	mongoose.connection.on('error', error => console.log(error));
};
