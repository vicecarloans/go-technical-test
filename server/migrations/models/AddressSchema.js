const mongoose = require('mongoose');

const { Schema } = mongoose;

const AddressSchema = new Schema({
	address1: {
		type: String,
		required: true,
	},
	address2: {
		type: String,
	},
	city: {
		type: String,
		required: true,
	},
	province: {
		type: String,
		required: true,
	},
	zip: {
		type: String,
		required: true,
	},
	country: {
		type: String,
		required: true,
	},
});

module.exports = AddressSchema;
