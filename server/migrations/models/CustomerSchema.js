const mongoose = require('mongoose');

const { Schema } = mongoose;

const AddressSchema = require('./AddressSchema');

const CustomerSchema = new Schema({
	email: {
		type: String,
		required: true,
		validate: {
			validator: v => {
				return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
					v
				);
			},
			message: props => `${props.value} is not a valid email!`,
		},
	},
	firstName: {
		type: String,
		required: true,
	},
	lastName: {
		type: String,
		required: true,
	},
	addresses: [AddressSchema],
	shopifyId: {
		type: Number,
		required: true,
	},
});

mongoose.model('customer', CustomerSchema);
