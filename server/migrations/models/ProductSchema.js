const mongoose = require('mongoose');

const { Schema } = mongoose;

const ProductSchema = new Schema({
	productId: {
		type: Number,
		required: true,
		unique: true,
	},
	title: {
		type: String,
		required: true,
	},
	description: {
		type: String,
	},
	price: {
		type: String,
		required: true,
	},
	image: {
		type: String,
		required: true,
	},
	createdAt: {
		type: Date,
		default: Date.now(),
	},
	modifiedAt: {
		type: Date,
		default: Date.now(),
	},
});

ProductSchema.pre(/^update/, function(next) {
	const modifiedDate = Date.now();
	this.modifiedAt = modifiedDate;
	next();
});

mongoose.model('product', ProductSchema);
